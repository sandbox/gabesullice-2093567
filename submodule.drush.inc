<?php

function submodule_drush_command() {

  $items['submodule'] = array(
    'callback' => 'submodule_drush_submodule',
    'description' => 'Work with git submodules in the context of Drupal',
    'aliases' => array('sm'),
    'examples' => array(
      'drush submodule add some_module' => 'Clone some_module and checkout the latest recommended tag as a git submodule.',
    ),
    'arguments' => array(
      'action' => 'A specific action to take, i.e., add, remove, move, or customize',
      'options' => 'Specify command options. E.g. -a to provide an API access key or -d to specify a directory location.',
      'module' => 'A specific module to act on.',
    ),
    'options' => array(
      'privacy' => array(
        'description' => 'If you are customizing a module and are setting up its own repository, will it be public or private.',
        'example-value' => 'private',
      ),
      'url' => array(
        'description' => 'Specify a URL to clone the module from.',
        'example-value' => 'https://github.com/drush-ops/drush.git',
      ),
      'directory' => array(
        'description' => 'A directory relative to the superproject that contains your drupal installation.',
        'example-value' => 'includes/modules/custom/some_module_name',
      ),
      'access' => array(
        'description' => 'A GitHub API Access key. Required if you will be setting up a custom repo via drush.',
        'example-value' => 'xxxxx377e5xxxxxc08d2xxxxx968fcxxxxx35019',
      ),
      'tag' => array(
        'description' => 'A specific tag to checkout.',
        'example-value' => '7.x-1.0',
      ),
      'branch' => array(
        'description' => 'A specific branch to checkout.',
        'example-value' => '7.x-1.x',
      ),
      'commit' => array(
        'description' => 'Specify whether or not to commit after making a submodule update. Default is TRUE.',
        'example-value' => 'FALSE',
      ),
      'push' => array(
        'description' => 'Specify a push URL. If you specify nothing, this extension will attempt to determine if the module is not contrib. If it isn\'t contrib it will try to set up an appropriate push url.',
        'example-value' => 'http://git.drupal.org/project/admin_menu',
      ),
      'fetch' => array(
        'description' => 'Specify a fetch URL. If you specify nothing, this extension will attempt to get and set a read-only URL so that the project can be cloned without issue.',
        'example-value' => 'git@github.com:drush-ops/drush.git',
      ),
    ),
    'required-arguments' => FALSE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  return $items;
}

function submodule_drush_help_alter(&$command) {
  if ($command['command'] == 'submodule') {
    $command['options']['privacy'] = "In order to maintain custom changes to submodules, git requires that the submodule have its own repository. The submodule extension is built to automate the process of using git submodules and one of the steps it takes to accomplish this is working with the GitHub API to set up a new repository on your GitHub account. By default, it will set this repository up as a free, public one. However, you have the option to make it a private one by setting --privacy=private in your drush submodule customize command. Be sure that you have a paid account though, with available private repos before setting this option.";
    $command['options']['url'] = "Drush submodule can work like drush pm-download when you specify a contrib module on Drupal.org, finding the appropriate module and downloading the latest recommended release. However, it is possible to specify a specific clone URL. For example, this might be a clone URL for a custom module which you host on GitHub and use in-house";
    $command['options']['directory'] = "By default, drush submodule installs your submodules in includes/modules/contrib/some_)module_name (custom, if you specified a URL). However, you can specify an alternate location or directory name when you set this option.";
    $command['options']['access'] = "In order to automate the set-up of public or private repositories on your GitHub account, you will need to provide an API access key. For instructions one how to find and get this, please see the submodule project README.";
    $command['options']['tag'] = "You can specify a particular tag to checkout.";
    $command['options']['branch'] = "You can specify a particular branch to checkout. Unless you are confident that the branch will always be production-ready, it may be a better idea to specify a tag instead.";
    $command['options']['commit'] = "A drawback of git submouldes is that you can run into conflicts when changes aren't committed. This extensions default is to commit after actions. To change this, specify --commit=false.";
    $command['options']['push'] = "In order for anyone to be able to clone your superproject and have access to each of its submodules, it's necessary to ensure a valid read-only fetch URL. However, if this is the only URL provided for the module, you will not be able to push without specifying an accessible push URL. By default, this extension will attempt to get this URL and set it up.";
    $command['options']['fetch'] = "In order for anyone to be able to clone your superproject and have access to each of its submodules, it's necessary to ensure a valid read-only fetch URL. If one is not provided, this extension will attempt to provide and set a vaild push URL.";
  }
}

function submodule_drush_submodule() {

  /*
   * Validate command
   */

  $options = _submodule_build_option_array();
  $arguments = drush_get_arguments();

  // Validate correct action argument
  if (!preg_match("(^add$|^remove$|^move$|^customize$|^test$)", $arguments[1])) {
    drush_set_error("You must specify either add, remove, move, or customize.\nExample: drush submodule add views");
    return;
  }

  // Validate that only a module name or URL was provided.
  if ((isset($options['url']) && isset($arguments[2])) || (!isset($options['url']) && !isset($arguments[2]))) {
    drush_set_error("You must either specify a download URL OR a Drupal.org project name.");
    return;
  }

  switch ($arguments[1]) {
    case "add":
      submodule_add_submodule($options, $arguments);
      break;
    case "remove":

      if (drush_confirm("Proceeding with this command will permanently remove all the files of the specified submodule and will also remove any references to it in .gitmodules, .git/config, etc. You will lose anything that has not been copied elsewhere or pushed to a remote repository that you have access to. Do you still wish to continue?")) {
        if (drush_confirm("Are you sure?")) {
          echo "You're a brave, brave soul.\n";
        }
        else {
          break;
        }
      }
      else {
        break;
      }

    case "move":

      break;

    case "customize":
      if ($options['privacy'] == 'private') {
        if (drush_confirm("You've specified that you would like to set up a new PRIVATE repository. Please confirm that you have a GitHub account with at least one available private repository. Do you still wish to continue?")) {
          return;
        }
      }
      break;

  }
  /*
   * Prep work
   */

  /*
   * Actions
   */

  /*
   * Cleanuo
   */
}

/*
 * Action functions
 */

function submodule_add_submodule($options, $arguments) {
  if (isset($arguments[2])) {
    $url = "http://git.drupal.org/project/$arguments[2].git";
    $repo_name = $arguments[2];
    $repo_loc = $options['directory'] . $repo_name;
  }
  else {
    $url = $options['url'];
    $repo_name = preg_match("([a-zA-Z_\-0-9]*(?=.git$))", $url);
    $repo_loc = $options['directory'] . $repo_name;
  }

  $cmd_array = array(
    'base' => 'git submodule add',
//    'branch' => '--branch ' . $options['branch'],
    'url' => $url,
    'directory' => $repo_loc,
  );

  $cmd = '';

  foreach ($cmd_array as $part) {
    if (isset($part)) {
      $cmd .= $part . " ";
    }
  }

  echo $cmd . "\n";

  drush_shell_exec($cmd);

  if (!empty($options['branch'])) {
    drush_shell_cd_and_exec($repo_loc, "git checkout " . $options['branch']);
    drush_shell_exec("git config -f .gitmodules submodule.$repo_loc.branch " . $options['branch']);
  }

  if (!empty($options['tag'])) {
    drush_shell_cd_and_exec($repo_loc, "git checkout " . $options['tag']);
    drush_shell_exec("git config -f .gitmodules submodule.$repo_loc.tag " . $options['tag']);
  }

  if ($options['commit']) {
    drush_shell_exec("git commit -m 'Added submodule to " . $repo_loc . "' " . $repo_loc . " .gitmodules");
  }
}


/*
 * Helper functions
 */

function _submodule_build_option_array() {

  $options = array();

  $possible_options = array(
    'privacy' => array(
      'default' => 'public',
    ),
    'url' => array(
      'default' => NULL,
    ),
    'directory' => array(
      'default' => 'includes/modules/contrib/',
    ),
    'access' => array(
      'default' => NULL,
    ),
    'tag' => array(
      'default' => NULL,
    ),
    'branch' => array(
      'default' => NULL,
    ),
    'commit' => array(
      'default' => TRUE,
    ),
    'push' => array(
      'default' => NULL,
    ),
    'fetch' => array(
      'default' => NULL,
    ),
  );

  if (drush_get_option('url') && !drush_get_option('directory')) {
    $possible_options['directory']['default'] = 'includes/modules/custom/';
  }

  if (preg_match("(^([fF][aA][lL][sS][eE])$)", drush_get_option('commit'))) {
    drush_set_option('commit', FALSE);
  }

  $url = drush_get_option('url');

  if (!empty($url)) {

    if (preg_match("(git@github.com)", $url)) {

      preg_match("(:(.*)(?=\.git$))", $url, $matches = array());

      $repo = $matches[0];

      $repo = str_replace(':', '', $repo);

      $urls = _submodule_github_get_urls($repo);

      $possible_options['push']['default'] = $urls->ssh_url;
      $possible_options['fetch']['default'] = $urls->clone_url;
    }
  }

  foreach ($possible_options as $key => $option) {
    $options[$key] = drush_get_option($key, $option['default']);
  }

  return $options;
}

function _submodule_github_get_urls($repo) {
  drush_shell_exec("curl -o github.json https://api.github.com/repos/" . $repo);

  $json = file_get_contents('./github.json', FILE_USE_INCLUDE_PATH);

  $decoded = json_decode($json);

  drush_shell_exec("rm github.json");

  $urls = array(
    'clone_url' => $decoded->clone_url,
    'ssh_url' => $decoded->ssh_url,
  );

  return $urls;
}